Easy way to invoke macOS shell scripts via Siri voice commands

## Installation

Double-click `Run Script.shortcut` and select `Add Shortcut`

Hit the little play button on the upper right of the `Run Script` shortcut once you add it

Then in the `Which One?` box type `Chrome` and press `Done`

This should give you an `Allow 'Run Script' to run an AppleScript` prompt. Select `Allow`

In macOS `System Settings > Siri and Spotlight` enable the following:

- `Siri`
- `Listen for Siri`

## Usage

Say `Siri, Run Script`

It should say `Which One?`

Then you can say `Chrome` or `Quit Chrome`, or the name of any other script you place in `~/siri-run-script/scripts`

## Adding your script(s)

If you want to be able to invoke `My Script.sh` via Siri:

- place your `My Script.sh` file in `~/siri-run-script/scripts`
- say `Siri, Run Script`
- when Siri reponds `Which One?`, say `My Script` and it will run
