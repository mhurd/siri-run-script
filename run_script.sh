#!/bin/bash

SCRIPT_DIR=$(dirname "$0")

log() {
    echo -e "$1" >> "${SCRIPT_DIR}/log.txt"
}

main() {
    local input_command="$1"
    local lowercase_command=$(echo "$input_command" | tr '[:upper:]' '[:lower:]')
    local script_file="$SCRIPT_DIR/scripts/${lowercase_command}.sh"

    log "\nYou said to run: $input_command"
    log "$script_file"

    if [[ -x "$script_file" ]]; then
        echo "Running: $input_command"
        "$script_file"
    else
        local failure_message="$input_command script not found"
        echo "$failure_message"
        log "$failure_message"
    fi
}

main "$@"
