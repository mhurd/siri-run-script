-- Example usage:
--   osascript "$(dirname "$0")/open_url.applescript" "ChatGPT" "https://chat.openai.com"
-- Finds ChatGPT if it's already present in a Safari tab and brings it to front
-- Opens Safari loading ChatGPT if Safari wasn't running
on run argv
    set appName to item 1 of argv
    set targetBaseURL to item 2 of argv
	tell application "System Events"
	    set safariRunning to (name of processes) contains "Safari"
	end tell
	if not safariRunning then
	    tell application "Safari"
	        make new document with properties {URL:targetBaseURL}
	        activate
	        return "Started Safari and opened " & appName & " tab"
	    end tell
	else
	    tell application "Safari"
	        set urlFound to false
	        repeat with w from 1 to count of windows
	            set theWindow to window w
	            repeat with t from 1 to count of tabs of theWindow
	                set currentTabURL to URL of tab t of theWindow
	                if currentTabURL starts with targetBaseURL then
	                    tell theWindow to set current tab to tab t
	                    activate
	                    set index of theWindow to 1 -- brings the window to the front
	                    set urlFound to true
	                    exit repeat
	                end if
	            end repeat
	            if urlFound then exit repeat
	        end repeat
	        if not urlFound then
	            tell window 1 to set current tab to (make new tab with properties {URL:targetBaseURL})
	            activate
	            return "Opened new " & appName & " tab"
	        else
	            return "Found " & appName & " tab"
	        end if
	    end tell
	end if
end run
