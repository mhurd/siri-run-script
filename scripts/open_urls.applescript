-- Example usage:
--   osascript "$(dirname "$0")/open_urls.applescript" "https://google.com" "https://chat.openai.com"
-- Opens a new Safari window then opens each url in a tab in that new window
-- Does not look for already-opened urls like "open_url.applescript" does
on run argv
    -- Check if any URLs were provided
    if (count of argv) = 0 then
        display dialog "No URLs provided. Please pass one or more URLs as arguments." buttons {"OK"} default button "OK"
        return
    end if
    -- Initialize the list of URLs
    set urlList to argv
    tell application "Safari"
        activate
        -- Create a new window
        make new document
        -- Wait briefly for the window to initialize
        delay 0.5
        -- Get the current window count
        set windowCount to count of windows
        -- Focus on the new window (which should be the last one)
        set index of window windowCount to 1
        -- Open URLs in the new window
        repeat with theURL in urlList
            tell window 1
                set current tab to (make new tab with properties {URL:theURL})
            end tell
        end repeat
        -- Close the initial empty tab if it exists
        if (count of tabs of window 1) > (count of urlList) then
            close tab 1 of window 1
        end if
    end tell
end run