#!/bin/bash

output=$(/usr/local/bin/docker system prune -af)
deleted_images=$(echo "$output" | grep -c "^deleted: sha256:")
reclaimed_space=$(echo "$output" | grep "Total reclaimed space" | awk '{print $4}')
summary="Deleted $deleted_images images saving $reclaimed_space"

echo "$summary"