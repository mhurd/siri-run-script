#!/bin/bash

echo $(osascript "$(dirname "$0")/open_url.applescript" "Quickstart" "https://gitlab.wikimedia.org/mhurd/mediawiki-quickstart")

open ~/mediawiki-quickstart