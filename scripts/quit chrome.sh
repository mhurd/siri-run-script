#!/bin/bash

if pgrep "Google Chrome" > /dev/null; then
    osascript -e 'tell application "Google Chrome" to quit' > /dev/null 2>&1
else
  echo "Chrome wasn't running"
fi
